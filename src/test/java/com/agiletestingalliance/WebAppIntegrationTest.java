package com.agiletestingalliance;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.io.FileUtils;

public class WebAppIntegrationTest {
    private WebDriver driver;
    public String hubURL = null;
    public String testURL = null;
    public String ssl = null;

    @Before
    public void setUp() throws Exception {
        hubURL = System.getProperty("hubURL");
        testURL = System.getProperty("testURL");
        ssl = System.getProperty("screenshotLocation");

        final DesiredCapabilities caps = new DesiredCapabilities();
        caps.setBrowserName("chrome");
        driver = new RemoteWebDriver(new URL(hubURL), caps);
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void testScreenshot() throws Exception {
        driver.get(testURL);
        captureScreenshot(driver, ssl);
        assertEquals("ScreenShot", 8, 8);
    }

    public void captureScreenshot(final WebDriver driver, final String filename) throws IOException {
		final File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File(filename));
	}
}
