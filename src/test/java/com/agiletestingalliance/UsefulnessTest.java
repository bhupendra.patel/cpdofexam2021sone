package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class UsefulnessTest {
    @Test
    public void testDesc() throws Exception {
        final String textInAbout = new Usefulness().desc();
        assertTrue("UseFulNess Test", textInAbout.toString().contains("DevOps is about transformation"));
        
    }

    @Test
    public void testFunctionWF() throws Exception {
        new Usefulness().functionWF();
        assertEquals("UseFulNess Test 2", 15, 15);
    }
}
