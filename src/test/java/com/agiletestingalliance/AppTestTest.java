package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class AppTestTest {
    @Test
    public void testGSTR() throws Exception {
        final String textInAbout = new AppTest("Bhupendra Patel Is In Universe").gstr();
        assertTrue("App Test", textInAbout.toString().contains("Bhupendra Patel"));
        
    }
    
}
