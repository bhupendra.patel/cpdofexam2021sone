package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {
    @Test
    public void testDesc() throws Exception {
        final String textInAbout = new AboutCPDOF().desc();
        assertTrue("AboutPage", textInAbout.toString().contains("CP-DOF certification"));
        
    }
}
