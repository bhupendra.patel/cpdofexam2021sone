package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;


public class MinMaxTest {
    @Test
    public void testBar() throws Exception {
        final String textInAbout = new MinMax().bar("Testing this function");
        assertTrue("Min Test", textInAbout.toString().contains("Testing"));
        
    }

    @Test
    public void testFoo() throws Exception {
        final int value = new MinMax().foo(10, 4);
        assertEquals("MinMax Test", 10, value);
        
    }
    
    @Test
    public void testFooSec() throws Exception {
        final int value = new MinMax().foo(4, 5);
        assertEquals("MinMax Test", 5, value);
        
    }
}
