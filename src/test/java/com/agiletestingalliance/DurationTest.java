package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class DurationTest {
    @Test
    public void testDur() throws Exception {
        final String textInAbout = new Duration().dur();
        assertTrue("Duration Test", textInAbout.toString().contains("CP-DOF is designed specifically"));
        
    }

    @Test
    public void testCalculateIntValue() throws Exception {
        final int value = new Duration().calculateIntValue();
        assertEquals("Duration Test 2", 15, value);
        
    }
}
